<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function(){
    Route::post('login', 'User\Auth\LoginCn@login')->name('auth.login');
    Route::post('register', 'User\Auth\RegisterCn@register')->name('auth.register');
    Route::post('logout', 'User\Auth\LogoutCn@logout')->name('auth.logout');
});

Route::group(['prefix' => 'user'], function(){
    Route::get('profile/{id}', 'User\ProfileCn@index')->name('user.profile');
    Route::put('profile-update/{id}', 'User\ProfileCn@update')->name('user.profile.update');
});
