// https://appdividend.com/2018/11/17/vue-laravel-crud-example-tutorial-from-scratch/

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);
// Vue.axios.defaults.baseURL = `http://${process.env.host}:${process.env.port}`;
// Vue.axios.defaults.baseURL = 'http://localhost:8000/api/';


import Login from './components/auth/Login.vue';
import Register from './components/auth/Register.vue';

import Dashboard from './components/user/Dashboard.vue';
import Profile from './components/user/Profile.vue';
import ProfileEdit from './components/user/ProfileEdit.vue';

const routes = [
    {
        name: '/',
        path: '/',
        component: Login
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'register',
        path: '/register',
        component: Register
    },
    {
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard
    },
    {
        name: 'profile',
        path: '/profile',
        component: Profile
    },
    {
        name: 'profile-edit',
        path: '/profile-edit',
        component: ProfileEdit
    }
];

const router      = new VueRouter({ mode: 'history', routes: routes});
const app         = new Vue(Vue.util.extend({router}, App)).$mount('#app');
const API_ROOT    = 'http://localhost:8000/api/'; 

router.beforeEach((to, from, next) => {
    let usrId = localStorage.getItem('usrId');
    console.log(to);
    
    if(to.name == '/' || to.name == 'login' || to.name == 'register')
    {        
        if(usrId) {
            next({name: 'dashboard'});
        } else {
            next();
        }
        return false;
    }

    if(usrId) {
        next();
    } else {
        next({'name': 'login'});
    }
});