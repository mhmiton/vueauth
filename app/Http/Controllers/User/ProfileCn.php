<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Model\User;

class ProfileCn extends Controller
{
    public function index($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
    
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name          = $request->name;
        $user->country       = $request->country;
        $user->state         = $request->state;
        $user->city          = $request->city;
        $user->zip_code      = $request->zip_code;
        $user->address       = $request->address;

        $update = $user->save();
        
        return response()->json([
            'success' => true,
            'message' => 'Update successful.',
        ]);
    }

    public function destroy($id)
    {
        
    }
}