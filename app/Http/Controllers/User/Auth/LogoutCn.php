<?php

namespace App\Http\Controllers\User\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;

class LogoutCn extends Controller
{
    public function logout(Request $request)
    {
        auth()->guard('vue')->logout();
        return response()->json([
            'success' => true,
            'message' => 'Logout Successfull',
        ]);
    }
}