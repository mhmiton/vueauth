<?php

namespace App\Http\Controllers\User\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Model\User;

class RegisterCn extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'phone'     => 'required|numeric|digits_between:11,15|unique:users',
            'email'     => 'required|email|max:255|unique:users',
            'username'  => 'required|unique:users',
            'password'  => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation failed.',
                'errors' => $validator->errors()
            ]);
        }

        $user = new User;
        $user->name          = $request->name;
        $user->phone         = $request->phone;
        $user->email         = $request->email;
        $user->username      = $request->username;
        $user->password      = Hash::make($request->password);

        $save = $user->save();
        return response()->json([
            'success' => true,
            'message' => 'Signup successful.',
        ]);
    }
}