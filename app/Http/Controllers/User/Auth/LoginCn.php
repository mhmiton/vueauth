<?php

namespace App\Http\Controllers\User\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Model\User;


class LoginCn extends Controller
{
    public function isAuth()
    {
        dd(auth()->guard('vue')->user());
        if (auth()->check()) {
            return response()->json([
                'auth' => true,
                'message' => 'Authorized User.',
            ]);
        } else {
            return response()->json([
                'auth' => false,
                'message' => 'Unauthorized User.',
                'errors' => ['unauthorized' => ['Unauthorized User.']]
            ]);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation failed.',
                'errors' => $validator->errors()
            ]);
        }

        $username    = $request->username;
        $password    = $request->password;


        if (auth()->guard('vue')->attempt(['username' => $username, 'password' => $password])) {
            return response()->json([
                'success' => true,
                'message' => 'Login Successfull',
                'user' => auth()->guard('vue')->user(),
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Login Failed',
                'errors' => ['login_failed' => ['These Credentials Do Not Match Our Records.']]
            ]);
        }
    }
}